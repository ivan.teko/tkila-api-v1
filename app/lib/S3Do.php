<?php

namespace Boletiland\S3Do;

use Aws\S3\S3Client;

class S3Do {
    private $s3;
    private $bucket;
    private $prefix;

    public function __construct($endpoint, $region, $bucket, $accessKeyId, $secretAccessKey, $prefix) {
        $this->s3 = new S3Client(['version' => 'latest', 'region' => $region, 'credentials' => ['key' => $accessKeyId, 'secret' => $secretAccessKey], 'endpoint' => $endpoint]);
        $this->bucket = $bucket;
        $this->prefix = $prefix;
    }

    public function uploadFile($key, $filePath, $contentType = null, $acl = 'public-read'): array {
        try {
            return ['error' => false, 'res' => $this->s3->putObject(['Bucket' => $this->bucket, 'Key' => $this->prefix.$key, 'SourceFile' => $filePath, 'ACL' => $acl, 'ContentType' => $contentType])];
        } catch (\Exception $e) {
            return ['error' => true, 'message' => $e->getMessage()];
        }
    }

    public function updateFile($key, $filePath): array {
        try {
            // Primero, eliminamos el archivo existente
            $res = $this->deleteFile($key);
            if($res['error']){
                throw new \Exception('No se pudo eliminar el archivo en S3');
            }
            // Luego, subimos el nuevo contenido
            return ['error' => false, 'res' => $this->uploadFile($key, $filePath)];
        } catch (\Exception $e) {
            return ['error' => true, 'message' => $e->getMessage()];
        }
    }

    public function deleteFile($key): array {
        try {
            return ['error' => false, 'res' => $this->s3->deleteObject(['Bucket' => $this->bucket, 'Key' => $key])];
        } catch (\Exception $e) {
            return ['error' => true, 'message' => $e->getMessage()];
        }
    }

    public function uploadFileContents($key, $content, $contentType = null, $acl = 'public-read'): array {
        try {
            return ['error' => false, 'res' => $this->s3->putObject(['Bucket' => $this->bucket, 'Key' => $this->prefix.$key, 'Body' => $content, 'ACL' => $acl, 'ContentType' => $contentType])];
        } catch (\Exception $e) {
            return ['error' => true, 'message' => $e->getMessage()];
        }
    }

    public function getFileContents($key): array {
        try {
            $result = $this->s3->getObject(['Bucket' => $this->bucket, 'Key' => $key]);
            return ['error' => false, 'res' => $result['Body']->getContents()];
        } catch (\Exception $e) {
            return ['error' => true, 'message' => $e->getMessage()];
        }
    }

    public function updateFileContents($key, $newContent): array {
        try {
            // Primero, eliminamos el archivo existente
            $res = $this->deleteFile($key);
            if($res['error']){
                throw new \Exception('No se pudo eliminar el archivo en S3');
            }
            // Luego, subimos el nuevo contenido
            return ['error' => false, 'res' => $this->uploadFileContents($key, $newContent)];
        } catch (\Exception $e) {
            return ['error' => true, 'message' => $e->getMessage()];
        }
    }
}
