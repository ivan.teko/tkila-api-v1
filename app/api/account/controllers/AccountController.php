<?php

namespace app\api\account\controllers;

use Delight\Auth\DatabaseError;
use Delight\Auth\InvalidOneTimePasswordException;
use Delight\Auth\InvalidPasswordException;
use Delight\Auth\NotLoggedInException;
use Delight\Auth\TwoFactorMechanismAlreadyEnabledException;
use Delight\Auth\TwoFactorMechanismNotInitializedException;
use Exception;
use RobThree\Auth\TwoFactorAuthException;
use support\Db;
use Tkila\Routing\Controller;
use Tkila\Routing\Request;
use Tkila\Routing\Response;
use RobThree\Auth\TwoFactorAuth;

class AccountController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function changeCurrentPassword(Request $request): Response
    {
        try {
            $request = (object)$request->getParams();

            $response = tk_change_current_password($request->current_password, $request->password);

            if ($response["error"]) {
                return $this->response->message("An error occurred while trying to change the password.");
            }

            return $this->response->message("Password successfully changed.");
        } catch (NotLoggedInException $e) {
            return $this->response->error(true)->message("Not logged in");
        } catch (InvalidPasswordException $e) {
            return $this->response->error(true)->message("Invalid Password");
        } catch (Exception $e) {
            return $this->response->error(true)->message("Exception");
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function checkIfUserHas2FA(Request $request): Response
    {
        global $jwt_token;

        try {
            $data = Db::table("users_2fa_google")->select("id_user", "verified")->where("id_user", $jwt_token->id_user)->first();

            if (!is_null($data)) {
                return $this->response->message("Information queried correctly")->data($data);
            } else {
                return $this->response->error(true)->message("No information found")->data([]);
            }
        } catch (Exception $e) {
            return $this->response->error(true)->message("Authentication error");
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getQRCode(Request $request): Response
    {
        global $jwt_token, $tekoconfig;

        try {
            $tfa = new TwoFactorAuth();

            $request = (object)$request->getParams();
            $user = Db::table("users")->select("email")->where("id", $jwt_token->id_user)->first();

            $username = $user->email;
            $pass = $request->pass;

            $res = tk_login($username, $pass, false);

            if ($res["error"]) {
                return $this
                    ->response
                    ->error(true)
                    ->message($res["message"]);
            } else {
                $has_2fa = Db::table("users_2fa_google")->where("id_user", $jwt_token->id_user)->first();

                $secret = $tfa->createSecret();

                if (is_null($has_2fa)) {
                    Db::table("users_2fa_google")->insert(
                        [
                            "id_user" => $jwt_token->id_user,
                            "seed" => $secret,
                            "verified" => 0,
                            "created_at" => date("Y-m-d H:i:s"),
                        ]
                    );
                } else {
                    Db::table("users_2fa_google")->where("id_user", $jwt_token->id_user)->update(["seed" => $secret,]);
                }

                return $this->response->message("QR queried successfully")->data([
                    "qr_data" => $tfa->getQRCodeImageAsDataUri("{$tekoconfig["site"]["name"]}: $username", $secret)
                ]);
            }
        } catch (TwoFactorAuthException $e) {
            return $this->response->error(true)->message("Two factor authentication failed");
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function enable2FA(Request $request): Response
    {
        global $jwt_token;

        try {
            $tfa = new TwoFactorAuth();

            $request = (object)$request->getParams();
            $user = Db::table("users")->select("email")->where("id", $jwt_token->id_user)->first();
            $has_2fa = Db::table("users_2fa_google")->where("id_user", $jwt_token->id_user)->first();

            $username = $user->email;
            $pass = $request->pass;
            $code = $request->code;

            $res = tk_login($username, $pass, false);

            if ($res["error"]) {
                return $this
                    ->response
                    ->error(true)
                    ->message($res["message"]);
            } else {
                $response = $tfa->verifyCode($has_2fa->seed, $code);

                if ($response) {
                    Db::table("users_2fa_google")->where("id_user", $jwt_token->id_user)->update(["verified" => 1]);

                    return $this->response->message("2FA enabled successfully");
                } else {
                    return $this->response->error(true)->message("An error ocurred when trying to verify code");
                }
            }
        } catch (TwoFactorAuthException $e) {
            return $this->response->error(true)->message("Two factor authentication failed");
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function remove2FA(Request $request): Response
    {
        global $jwt_token;

        try {
            $affected_2fa = Db::table("users_2fa_google")
                ->where("id_user", $jwt_token->id_user)
                ->delete();

            if ($affected_2fa > 0) {
                return $this->response->error(false)->message("2FA removed successfully");
            } else {
                return $this->response->error(true)->message("An error occurred while trying to delete 2FA and OTPS");
            }
        } catch (Exception $e) {
            return $this->response->error(true)->message("An exception occurred while trying to remove 2FA");
        }
    }
}