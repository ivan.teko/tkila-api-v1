<?php

namespace app\api\users\controllers;

use Exception;
use support\Db;
use Tkila\Routing\Controller;
use Tkila\Routing\Request;
use Tkila\Routing\Response;

class UsersController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function add(Request $request): Response
    {
        try {
            $request = (object)$request->getParams();

            $respuesta = tk_register(
                $request->email,
                $request->password,
                $request->role,
                $request->username,
                false,
                [
                    "name" => $request->name,
                    "lastname" => $request->lastname,
                    "phone" => $request->phone,
                    "status" => 1
                ]
            );

            if (isset($respuesta["error"])) {
                return $this
                    ->response
                    ->error(false)
                    ->message("Registro guardado correctamente");
            } else {
                return $this
                    ->response
                    ->error(true)
                    ->message("Ocurrio un error al guardar el registro");
            }
        } catch (Exception $ex) {
            return $this
                ->response
                ->error(true)
                ->message($ex->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function update(Request $request): Response
    {
        try {
            Db::beginTransaction();
            $request = (object)$request->getParams();

            if (!empty($request->password)) {
                tk_change_password($request->password, $request->id);
            }

            $affected = Db::table("users",)
                ->where("id", $request->id)
                ->update([
                    "name" => $request->name,
                    "lastname" => $request->lastname,
                    "username" => $request->username,
                    "role" => $request->role,
                    "email" => $request->email,
                    "phone" => $request->phone,
                ]);

            if ($affected > 0) {
                Db::commit();

                return $this
                    ->response
                    ->error(false)
                    ->message("Registro guardado correctamente");
            } else {
                Db::rollback();

                return $this
                    ->response
                    ->error(true)
                    ->message("Ocurrio un error al guardar el registro");
            }
        } catch (Exception $ex) {
            Db::rollback();

            return $this
                ->response
                ->error(true)
                ->message($ex->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function delete(Request $request): Response
    {
        try {
            Db::beginTransaction();
            $request = (object)$request->getParams();

            $affected = Db::table("users",)
                ->where("id", $request->id)
                ->delete();

            if ($affected > 0) {
                Db::commit();

                return $this
                    ->response
                    ->error(false)
                    ->message("Registro eliminado correctamente");
            } else {
                Db::rollback();

                return $this
                    ->response
                    ->error(true)
                    ->message("Ocurrio un error al eliminar el registro");
            }
        } catch (Exception $ex) {
            Db::rollback();

            return $this
                ->response
                ->error(true)
                ->message($ex->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getAll(Request $request): Response
    {
        try {
            $request = (object)$request->getParams();

            $data = Db::table("users")
                ->select(["id", "name", "lastname", "username", "role", "email", "phone"])
                /**
                 * Search values
                 */
                ->where((function ($query) use ($request) {
                    $query->where("name", "like", "%" . $request->search . "%");
                    $query->orWhere("lastname", "like", "%" . $request->search . "%");
                }))
                ->whereNot("role", "superadmin")
                /**
                 * Order By
                 */
                ->orderBy($request->order_by == null ? "name" : $request->order_by, $request->order_by == null ? "asc" : ($request->desc == "true" ? "desc" : "asc"))
                /**
                 * Paginate Results
                 */
                ->paginate($request->per_page, ["*"], "page", $request->page);

            if (!empty($data)) {
                return $this
                    ->response
                    ->error(false)
                    ->message("Información consultada correctamente")
                    ->data($data);
            } else {
                return $this
                    ->response
                    ->error(true)
                    ->message("No hay resultados")
                    ->data([]);
            }
        } catch (Exception $ex) {
            return $this
                ->response
                ->error(true)
                ->message($ex->getMessage());
        }
    }
}