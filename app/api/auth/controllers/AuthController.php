<?php

namespace app\api\auth\controllers;

use app\api\auth\requests\LoginRequest;
use app\api\auth\requests\RecoveryRequest;
use app\api\auth\requests\SignUpRequest;
use app\models\User;
use RobThree\Auth\TwoFactorAuth;
use support\Db;
use Tkila\Routing\Controller;
use Tkila\Routing\Exceptions\ValidationErrorException;
use Tkila\Routing\Request;
use Tkila\Routing\Response;
use AuthJWT;
use Exception;

class AuthController extends Controller
{
    /**
     * Login function.
     * @param LoginRequest $request the login request object
     * @return Response $this
     */
    public function login(LoginRequest $request): Response
    {
        try {
            $username = $request->username;
            $pass = $request->pass;
            $remember = isset($request->remember) ? $request->remember : false;

            $res = tk_login($username, $pass, $remember);

            if ($res["error"]) {
                return $this
                    ->response
                    ->error(true)
                    ->message($res["message"]);
            } else {
                $current_user = tk_current_user();
                $has_2fa = Db::table("users_2fa_google")->where("id_user", $current_user->id)->first();

                if (!is_null($has_2fa) && $has_2fa->verified == 1) {
                    $data = [
                        "has_2fa" => true,
                    ];

                    return $this->response
                        ->customData(
                            [
                                "data" => $data,
                            ]
                        )
                        ->message(__("Datos correctos, redireccionando a la autenticación en dos pasos..."));
                } else {
                    $data = [
                        "has_2fa" => false,
                    ];

                    $user = [
                        "id_user" => $current_user->id,
                        "username" => $current_user->username,
                        "name" => $current_user->name,
                        "lastname" => $current_user->lastname,
                        "role" => $current_user->role,
                        "email" => $current_user->email
                    ];
                    $token = AuthJWT::SignIn($user);

                    return $this->response
                        ->customData(
                            [
                                "token" => $token,
                                "user" => $user,
                                "data" => $data,
                            ]
                        )
                        ->message(__("Datos correctos, redireccionando a la pantalla principal..."));
                }
            }
        } catch (Exception $e) {
            return $this->response->error(true)->message($e->getMessage())->data($e->getTrace());
        }
    }

    /**
     * Login function.
     * @param Request $request the login request object
     * @return Response $this
     */
    public function login2FA(Request $request): Response
    {
        try {
            $tfa = new TwoFactorAuth();

            $request = (object)$request->getParams();

            $username = $request->user["username"];
            $pass = $request->user["pass"];
            $code = $request->model["code"];

            $res = tk_login($username, $pass, false);

            if ($res["error"]) {
                throw new Exception($res["message"]);
            } else {
                $current_user = tk_current_user();
                $has_2fa = Db::table("users_2fa_google")->where("id_user", tk_id())->first();
                $response = $tfa->verifyCode($has_2fa->seed, $code);

                if ($response) {
                    $user = [
                        "id_user" => $current_user->id,
                        "username" => $current_user->username,
                        "name" => $current_user->name,
                        "lastname" => $current_user->lastname,
                        "role" => $current_user->role,
                        "email" => $current_user->email
                    ];
                    $token = AuthJWT::SignIn($user);

                    return $this->response
                        ->customData(
                            [
                                "token" => $token,
                                "user" => $user,
                            ]
                        )
                        ->message(__("Datos correctos, redireccionando a la pantalla principal..."));
                } else {
                    return $this->response->error(true)->message("An error ocurred when trying to verify code");
                }
            }
        } catch (Exception $e) {
            return $this->response->error(true)->message($e->getMessage());
        }
    }

    /**
     * @param SignUpRequest $request
     * @param $View
     * @param $Scope
     * @return Response
     */
    public function signUp(SignUpRequest $request, $View, $Scope): Response
    {
        global $tekoconfig;
        $user = (object)$request->user;
        $usuario = $user->username;
        $email = $user->email;
        $nombre = $user->name;
        $apellidos = $user->lastname;
        $pass = $user->password;
        $role = "usuario";

        $res = tk_register($email, $pass, $role, $usuario, TRUE, ["name" => $nombre, "lastname" => $apellidos]);

        if ($res["error"]) {
            return $this->response->error(true)->message($res["message"]);
        } else {
            $token = $res["token"];
            $Scope->usuario = $usuario;
            $Scope->email = $email;
            $Scope->token = $token;
            $Scope->titulo = __("Confirma tu correo electrónico");
            $Scope->fecha = date("d/m/Y h:i A");
            $Scope->url_activacion = site_url("api/auth/activate/" . $Scope->token);
            $Scope->nombre_proyecto = $tekoconfig["site"]["name"];

            $View->config(array("layout" => "templates/mail"));
            $html = $View->render("mail/sign-up");

            //Envio de correo
            teko_mail($email, $Scope->titulo . " | " . $tekoconfig["site"]["name"], $html);

            return $this->response->message(__("Se te envió un correo de confirmación, revisa tu bandeja de entrada por favor."));
        }

    }

    /**
     * @param Request $request
     * @return Response
     */
    public function logout(Request $request): Response
    {
        tk_logout();

        return $this
            ->response
            ->error(false)
            ->message("Se ha cerrado la sesión correctamente");
    }

    /**
     * Recovery password function.
     * @param RecoveryRequest $request The request object containing the token and password.
     * @return Response
     */
    public function recoveryPassword(RecoveryRequest $request): Response
    {
        $token = $request->token;
        $pass = $request->pass;

        if (empty($token) || empty($pass)) {
            return $this->response->error(true)->message(__("No se recibieron los datos necesarios"));
        }

        $res = tk_reset_password($token, $pass);

        if ($res["error"]) {
            return $this->response->error(true)->message($res["message"]);
        } else {
            return $this->response->message(__("La contraseña fue cambiada con éxito, ahora puedes ingresar nuevamente."));
        }
    }

    /**
     * @param Request $request
     * @param $View
     * @param $Scope
     * @return Response
     * @throws ValidationErrorException
     */
    public function requestRecovery(Request $request, $View, $Scope): Response
    {
        global $tekoconfig;

        $request->rules([
            "email" => "required|email"
        ])->messages([
            "email" => "El correo es requerido"
        ])->validate();

        $email = $request->email;

        $res = tk_recover($email);
        $data_user = User::getInstance()->getBy("email", $email);

        if ($res["error"]) {
            return $this->response->message($res["message"])->error(true);
        } else {
            $token = $res["token"];
            $Scope->email = $email;
            $Scope->token = $token;
            $Scope->titulo = "Recuperar contraseña";
            $Scope->fecha = date("d/m/Y h:i A");
            $Scope->usuario = $data_user;
            $Scope->url_activacion = getFrontEndURL("/recovery-password/" . $Scope->token);
            $Scope->nombre_proyecto = $tekoconfig["site"]["name"];

            $View->config(array("layout" => "templates/mail"));
            $html = $View->render("mail/recovery");


            $sent = teko_mail($email, $Scope->titulo . " | " . $tekoconfig["site"]["name"], $html);

            if (!$sent) {
                return $this->response->message(__("No se pudo enviar el correo de recuperación, intenta nuevamente por favor."))->error(true);
            }

            return $this->response->message(__("Se te envió un correo de recuperación, revisa tu bandeja de entrada por favor."));
        }
    }

    /**
     * @param Request $request
     * @return Response
     * @throws ValidationErrorException
     */
    public function validate(Request $request): Response
    {
        $request->rules([
            "token" => "required"
        ])->messages([
            "token" => "El token es requerido"
        ])->validate();

        try {
            AuthJWT::Check($request->token);

            $user_data = AuthJWT::GetData($request->token);
            $current_user = User::getInstance()->getById($user_data->id_user);

            $user = [
                "id_user" => $current_user->id,
                "username" => $current_user->username,
                "name" => $current_user->name,
                "lastname" => $current_user->lastname,
                "role" => $current_user->role,
                "email" => $current_user->email
            ];
            return $this->response->error(false)->customData(["user" => $user]);
        } catch (Exception $ex) {
            return $this->response->error(true)->message($ex->getMessage())->customData(["reauth" => true]);
        }
    }
}