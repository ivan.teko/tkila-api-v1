<?php

namespace app\api\auth\requests;

use Tkila\Routing\Request;

/**
 * @property string $token
 * @property string $pass
 */
class RecoveryRequest extends Request
{
    protected $messages = [
        "token" => "El token es requerido",
        "pass" => "La nueva contraseña es requerida"
    ];

    protected $rules = [
        "token" => "required",
        "pass" => "required"
    ];
}