<?php

namespace app\api\auth\requests;

use Tkila\Routing\Request;

/**
 * @property string $username
 * @property string $pass
 */
class LoginRequest extends Request
{
    protected $messages = [
        "username" => "El nombre de usuario o correo es requerido",
        "pass" => "La contraseña es requerida"
    ];

    protected $rules = [
        "username" => "required",
        "pass" => "required"
    ];
}