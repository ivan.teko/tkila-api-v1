<?php

namespace app\api\auth\requests;

use Tkila\Routing\Request;

/**
 * @property object $user
 */
class SignUpRequest extends Request
{
    protected $messages = [
        "user" => "Los datos del usuario son reququeridos",
        "user.username" => "El nombre de usuario es requerido",
        "user.email" => "El email es requerido",
        "user.name" => "El nombre es requerido",
        "user.lastname" => "El apellido es requerido",
        "user.password" => "La contraseña es requerida",
    ];

    protected $rules = [
        "user" => "required",
        "user.username" => "required",
        "user.email" => "required|email",
        "user.name" => "required",
        "user.lastname" => "required",
        "user.password" => "required",
    ];
}