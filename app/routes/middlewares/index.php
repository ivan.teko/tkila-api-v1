<?php

use Firebase\JWT\ExpiredException;
use Firebase\JWT\SignatureInvalidException;
use Tipsy\Tipsy;

//Header de acceso para ruta cualquier ruta /api/*
Tipsy::middleware(function($Request) {
    global $ruta, $jwt_token;
    //URL PATH
    $ruta = $Request->path();
    $public_paths = ['i18n','auth'];
    if ($Request->loc() == 'api') {
        //CORS Headers
        putCORS();
        //Public Paths
        if(!in_array($Request->loc(1), $public_paths)){
            $headers = $Request->headers();
            //Fix empty authorization
            if(!empty($headers['authorization'])){
                $headers['Authorization'] = $headers['authorization'];
            }
            if(!empty($headers['Authorization'])){
                list($token) = sscanf($headers['Authorization'], 'Bearer %s');
                //Check Token
                try {
                    AuthJWT::Check($token);
                    //Si el token es correcto, permite el acceso a la ruta y lo guarda decodificado en $jwt_token
                    $jwt_token = AuthJWT::GetData($token);
                } catch(ExpiredException $ex){
                    teko_json(['error' => true, 'message' => 'Tu sesión ha expirado, ingresa nuevamente por favor', 'reauth' => true]);
                } catch(SignatureInvalidException $ex){
                    teko_json(['error' => true, 'message' => 'Token inválido, ingresa nuevamente por favor', 'reauth' => true]);
                } catch (Exception $ex){
                    teko_json(['error' => true, 'message' => $ex->getMessage(), 'reauth' => true]);
                }
            } else {
                teko_json(['error' => true, 'message' => 'Authorization token required', 'reauth' => true]);
            }
        }
    }
});