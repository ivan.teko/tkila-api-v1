<?php

use app\api\account\controllers\AccountController;
use Tkila\Routing\Router;

Router::AddRoute('POST', 'change-current-password', [AccountController::class, 'changeCurrentPassword']);
Router::AddRoute('POST', 'check-if-user-has-2fa', [AccountController::class, 'checkIfUserHas2FA']);
Router::AddRoute('POST', 'get-qr-code', [AccountController::class, 'getQRCode']);
Router::AddRoute('POST', 'enable-2fa', [AccountController::class, 'enable2FA']);
Router::AddRoute('POST', 'remove-2fa', [AccountController::class, 'remove2FA']);