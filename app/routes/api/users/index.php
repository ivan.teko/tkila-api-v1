<?php

use app\api\users\controllers\UsersController;
use Tkila\Routing\Router;

Router::AddRoute('POST', 'add', [UsersController::class, 'add']);
Router::AddRoute('POST', 'update', [UsersController::class, 'update']);
Router::AddRoute('POST', 'delete', [UsersController::class, 'delete']);
Router::AddRoute('GET', 'get-all', [UsersController::class, 'getAll']);