<?php

use app\api\auth\controllers\AuthController;
use Tkila\Routing\Router;

Router::AddRoute('POST', 'login', [AuthController::class, 'login']);
Router::AddRoute('POST', 'login-2fa', [AuthController::class, 'login2FA']);
Router::AddRoute('POST', 'logout', [AuthController::class, 'logout']);
Router::AddRoute('POST', 'sign-up', [AuthController::class, 'signUp']);
Router::AddRoute('POST', 'validate', [AuthController::class, 'validate']);
Router::AddRoute('POST', 'request-recovery', [AuthController::class, 'requestRecovery']);
Router::AddRoute('POST', 'recovery-password', [AuthController::class, 'recoveryPassword']);