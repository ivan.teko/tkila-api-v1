<?php

use Boletiland\S3Do\S3Do;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use support\Db;

/* Datetime zone configuration */
date_default_timezone_set('America/Mexico_City');

/* Money format */
setlocale(LC_MONETARY, 'es_MX');

/**
 * Function to get a date from unix format
 * @param $time
 * @param string $format
 * @return string
 */
function unix2local($time, string $format = 'd/m/Y h:i A'): string
{
    $dt = new DateTime("@{$time}");
    $dt->setTimeZone(new DateTimeZone('America/Mexico_City'));
    return $dt->format($format);
}

/**
 * Function to get the frontend url with a page
 * @param $page
 * @return string
 */
function getFrontEndURL($page): string
{
    global $tekoconfig;

    return trim($tekoconfig["frontend"]["url"] . $page, '/');
}

/**
 * @param $id_file
 * @return Model|Builder|object|null
 */
function getFile($id_file)
{
    $file = Db::table("files")->where("id", "=", $id_file)->first();

    if (!empty($file)) {
        $file->fullpath = getCDNFileURL($file->path);

        return $file;
    } else {
        return null;
    }
}

/**
 * Function to get S3 Instance
 * @return S3Do
 */
function getS3Instance(): S3Do
{
    global $tekoconfig;

    return new S3Do(
        $tekoconfig["s3_do"]["endpoint"],
        $tekoconfig["s3_do"]["region"],
        $tekoconfig["s3_do"]["bucket"],
        $tekoconfig["s3_do"]["accessKeyId"],
        $tekoconfig["s3_do"]["secretAccessKey"],
        $tekoconfig["s3_do"]["prefix"]
    );
}

/**
 * Function to upload files to S3
 * @param $instance
 * @param $key
 * @param $file
 * @return array
 */
function uploadFileS3($instance, $key, $file): array
{
    global $tekoconfig;

    $dst_name = "{$key}_{$file["name"]}";

    $response = $instance->uploadFile($dst_name, $file["tmp_name"], $file["type"]);

    if (!$response["error"]) {
        Db::table("files")->insert(
            [
                "id" => $key,
                "type" => $file["type"],
                "name" => $file["name"],
                "size" => $file["size"],
                "path" => $tekoconfig["s3_do"]["prefix"] . $dst_name,
                "created_at" => date("Y-m-d H:i:s"),
            ]
        );

        return array("error" => false, "message" => __('File uploaded successfully'));
    } else {
        return array("error" => true, "message" => $response["message"]);
    }
}

/**
 * @param $path
 * @return string
 */
function getCDNFileURL($path): string
{
    global $tekoconfig;

    return "{$tekoconfig["s3_do"]['cdn_url']}/$path";
}