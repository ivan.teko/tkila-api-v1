<?php

define('ABSPATH', dirname(__DIR__));
define('APPPATH', dirname(__FILE__));
define('ADMIN_URL', 'admin');
define('TK_VERSION', '1.0.0');

if (!file_exists(ABSPATH . '/vendor/autoload.php')) {
    require_once('teko-core/assets/no-composer/index.php');
    exit;
}

require_once ABSPATH . '/vendor/autoload.php';

//Namespace
use Tipsy\Tipsy;
use Gettext\Translator\Translator;
use Gettext\Translator\TranslatorFunctions;
use Gettext\Loader\PoLoader;
use Delight\Auth\Auth;
use Illuminate\Database\Capsule\Manager as Capsule;

\Tkila\Log::setDirectory(ABSPATH);

$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

//Tipsy
Tipsy::config(ABSPATH . '/config/config.ini');

//Global variables
global $tekodb, $tekoauth, $tekoconfig, $title, $site_url, $site_name, $sidebar_menu, $current_url, $current_user, $current_lang, $router;

$tekoconfig = Tipsy::config();

//i18n (Docs: https://github.com/oscarotero/Gettext)
$current_lang = !empty($tekoconfig['site']['lang']) ? $tekoconfig['site']['lang'] : 'es_MX';

$loader = new PoLoader();
$translations = $loader->loadFile(APPPATH . '/i18n/' . $current_lang . '/LC_MESSAGES/messages.po');
$t = Translator::createFromTranslations($translations);
TranslatorFunctions::register($t);

//Install
if (empty($tekoconfig['site']['path'])) {
    require_once('teko-core/assets/install/index.php');
    exit;
}

//Debug
if ($tekoconfig['debug']['show_errors']) {
    @ini_set('display_errors', 1);
    @ini_set('display_startup_errors', 1);
    @error_reporting(E_ALL);
} else {
    @ini_set('display_errors', 0);
    @ini_set('display_startup_errors', 0);
    @error_reporting(0);
}

//Cookie params (Auth)
$cookie_params = [
    'lifetime' => ((int)(60 * 60 * 24 * 365.25) + 100),
    'path' => $tekoconfig['site']['path'],
    'domain' => $_SERVER['HTTP_HOST'],
    'secure' => isSSL(),
    'httponly' => true
];
session_set_cookie_params($cookie_params['lifetime'], $cookie_params['path'], $cookie_params['domain'], $cookie_params['secure'], $cookie_params['httponly']);

//DB Config
$db_user = $tekoconfig['db']['user'];
$db_pass = $tekoconfig['db']['pass'];
$db_name = $tekoconfig['db']['name'];
$db_host = $tekoconfig['db']['host'];
$tekodb = new ezSQL_mysqli($db_user, $db_pass, $db_name, $db_host, 'utf8mb4');

$capsule = new Capsule;
$capsule->addConnection([
    "driver" => "mysql",
    "host" => $db_host,
    "database" => $db_name,
    "username" => $db_user,
    "password" => $db_pass,
    "charset" => "utf8mb4",
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();

//Site URL
$scheme = isSSL() ? 'https' : 'http';
if (!in_array($_SERVER['SERVER_PORT'], [80, 443])) {
    $site_url = trim("{$scheme}://" . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . $tekoconfig['site']['path'], '/');
} else {
    $site_url = trim("{$scheme}://" . $_SERVER['SERVER_NAME'] . $tekoconfig['site']['path'], '/');
}

//Site Name
$site_name = $tekoconfig['site']['name'];

//PDO Driver MySQL
try {
    $pdo = new PDO("mysql:dbname=$db_name;host=$db_host;charset=utf8mb4", $db_user, $db_pass, []);
} catch (Exception $ex) {
    require_once('teko-core/assets/nodb/index.php');
    exit;
}
//Checks if users table exists, otherwise create it
$users_table_exists = $tekodb->get_col("SHOW TABLES LIKE 'users'");
if (!count($users_table_exists)) {
    $sql = file_get_contents(APPPATH . '/teko-core/lib/delight-im/auth/Database/MySQL.sql');
    $pdo->exec($sql);
}
//Authentication global variable (Docs: https://github.com/delight-im/PHP-Auth)
$tekoauth = new Auth($pdo, null, null, false);

$current_user = tk_current_user();
if (!$current_user) {
    $current_user = (object)['id' => 0, 'role' => 'guest'];
}

//Global functions
require_once('functions.php');

//AuthJWT Class
require_once(APPPATH . '/lib/auth/AuthJWT.php');

//Router
$router = Tipsy::router();
require_once('routes.php');

//Ruta Actual
$current_url = !empty($_GET['__url']) ? $_GET['__url'] : '';

//Middlewares
require_once('routes/middlewares/index.php');

//Inicializar App
Tipsy::run();
