<?php

use Tkila\Routing\Router;

$router->home(function ($View) {
    global $title;
    $title = 'Home';
    $View->display('index');
});

$router->otherwise(function ($View, $Request, $Scope) {
    global $title;
    $title = '404';
    http_response_code(200);
    $View->config(array('layout' => 'templates/404'));
    $View->display('404');
});

Router::AddGroup('/api', function () {
    Router::AddGroup('/auth', __DIR__ . '/routes/api/auth/index.php');
    Router::AddGroup('/account', __DIR__.'/routes/api/account/index.php');
    Router::AddGroup('/users', __DIR__ . '/routes/api/users/index.php');
});

try {
    Router::Collect();
} catch (Exception $e) {
}