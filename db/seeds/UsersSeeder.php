<?php

declare(strict_types=1);

use Phinx\Seed\AbstractSeed;

class UsersSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $data = [
            'email'         => 'admin@tekoestudio.com',
            'password'      => '$2y$10$L//St5.EgyEChDkqT9vsN.Iam1um5pyAkNJTqnW4PtB3y7lpIO2zm',
            'username'      => 'admin.teko',
            'status'        => 1,
            'verified'      => 1,
            'resettable'    => 1,
            'roles_mask'    => 0,
            'registered'    => 1,
            'last_login'    => 1696147087,
            'force_logout'  => 0,
            'role'          => 'admin',
            'name'          => 'Admin',
            'lastname'      => 'Teko Estudio',
        ];

        $users = $this->table('users');
        $users->insert($data)
              ->saveData();
    }
}
