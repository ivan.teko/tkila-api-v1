<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateUsersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        // Creando la tabla usuarios
        $this->execute(
            "CREATE TABLE `users` (
                `id` int(10) UNSIGNED NOT NULL,
                `email` varchar(249) NOT NULL,
                `password` varchar(255) NOT NULL,
                `username` varchar(100) DEFAULT NULL,
                `status` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
                `verified` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
                `resettable` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
                `roles_mask` int(10) UNSIGNED NOT NULL DEFAULT '0',
                `registered` int(10) UNSIGNED NOT NULL,
                `last_login` int(10) UNSIGNED DEFAULT NULL,
                `force_logout` mediumint(7) UNSIGNED NOT NULL DEFAULT '0',
                `role` varchar(100) DEFAULT NULL,
                `name` varchar(255) DEFAULT NULL,
                `lastname` varchar(255) DEFAULT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        );

        $this->execute("
            ALTER TABLE `users`
                ADD PRIMARY KEY (`id`),
                ADD UNIQUE KEY `email` (`email`),
                ADD UNIQUE KEY `username` (`username`);
            
            ALTER TABLE `users`
                MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
        ");

        // Creando la tabla users_confirmations
        $this->execute("
            CREATE TABLE `users_confirmations` (
                `id` int(10) UNSIGNED NOT NULL,
                `user_id` int(10) UNSIGNED NOT NULL,
                `email` varchar(249) NOT NULL,
                `selector` varchar(16) NOT NULL,
                `token` varchar(255) NOT NULL,
                `expires` int(10) UNSIGNED NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            ALTER TABLE `users_confirmations`
                ADD PRIMARY KEY (`id`),
                ADD UNIQUE KEY `selector` (`selector`),
                ADD KEY `email_expires` (`email`,`expires`),
                ADD KEY `user_id` (`user_id`);

            ALTER TABLE `users_confirmations`
                MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
        ");

        //Creando la tabla users_remembered
        $this->execute("
            CREATE TABLE `users_remembered` (
                `id` bigint(20) UNSIGNED NOT NULL,
                `user` int(10) UNSIGNED NOT NULL,
                `selector` varchar(24) NOT NULL,
                `token` varchar(255) NOT NULL,
                `expires` int(10) UNSIGNED NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            ALTER TABLE `users_remembered`
                ADD PRIMARY KEY (`id`),
                ADD UNIQUE KEY `selector` (`selector`),
                ADD KEY `user` (`user`);

            ALTER TABLE `users_remembered`
                MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
        ");

        //Creando la tabla users_resets
        $this->execute("
            CREATE TABLE `users_resets` (
                `id` bigint(20) UNSIGNED NOT NULL,
                `user` int(10) UNSIGNED NOT NULL,
                `selector` varchar(20) NOT NULL,
                `token` varchar(255) NOT NULL,
                `expires` int(10) UNSIGNED NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
          
            ALTER TABLE `users_resets`
                ADD PRIMARY KEY (`id`),
                ADD UNIQUE KEY `selector` (`selector`),
                ADD KEY `user_expires` (`user`,`expires`);
            
            ALTER TABLE `users_resets`
                MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
        ");

        //Creando la tabla users_throttling
        $this->execute("
            CREATE TABLE `users_throttling` (
                `bucket` varchar(44) NOT NULL,
                `tokens` float UNSIGNED NOT NULL,
                `replenished_at` int(10) UNSIGNED NOT NULL,
                `expires_at` int(10) UNSIGNED NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            
            ALTER TABLE `users_throttling`
                ADD PRIMARY KEY (`bucket`),
                ADD KEY `expires_at` (`expires_at`);
        ");
    }
}
