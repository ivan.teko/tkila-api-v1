<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateUploadsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $this->execute(
            "CREATE TABLE `uploads` (
                `id_upload` int(10) UNSIGNED NOT NULL,
                `user_id` int(10) UNSIGNED NOT NULL,
                `name` varchar(255) NOT NULL,
                `path` varchar(255) NOT NULL,
                `type` varchar(255) NOT NULL,
                `size` int(10) UNSIGNED NOT NULL,
                `created_at` int(10) UNSIGNED NOT NULL,
                `updated_at` int(10) UNSIGNED NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            ALTER TABLE `uploads`
                ADD PRIMARY KEY (`id_upload`);

            ALTER TABLE `uploads`
                MODIFY `id_upload` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
        ");
    }
}
