<?php
require_once __DIR__ . '/vendor/autoload.php';

use Tipsy\Tipsy;

Tipsy::config(__DIR__ .'/config/config.ini');
$tekoconfig = Tipsy::config();

return
[
    'paths' => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
        'seeds' => '%%PHINX_CONFIG_DIR%%/db/seeds'
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_environment' => 'development',
        'production' => [
            'adapter' => 'mysql',
            'host' => $tekoconfig['db']['host'],
            'name' => $tekoconfig['db']['name'],
            'user' => $tekoconfig['db']['user'],
            'pass' => $tekoconfig['db']['pass'],
            'port' => $tekoconfig['db']['port'],
            'charset' => 'utf8',
        ],
        'development' => [
            'adapter' => 'mysql',
            'host' => $tekoconfig['db']['host'],
            'name' => $tekoconfig['db']['name'],
            'user' => $tekoconfig['db']['user'],
            'pass' => $tekoconfig['db']['pass'],
            'port' => $tekoconfig['db']['port'],
            'charset' => 'utf8',
        ],
        'testing' => [
            'adapter' => 'mysql',
            'host' => $tekoconfig['db']['host'],
            'name' => $tekoconfig['db']['name'],
            'user' => $tekoconfig['db']['user'],
            'pass' => $tekoconfig['db']['pass'],
            'port' => $tekoconfig['db']['port'],
            'charset' => 'utf8',
        ]
    ],
    'version_order' => 'creation'
];
